using HarmonyLib;
using PeterHan.PLib.Buildings;
using PeterHan.PLib.Core;
using PeterHan.PLib.UI;
using System.Collections.Generic;
using UnityEngine;
using static DetailsScreen;

namespace notfound404.AdvancedCritterSensor {
	public class AdvancedCritterSensorPatches : KMod.UserMod2 {
		public override void OnLoad(Harmony harmony) {
            base.OnLoad(harmony);
            PUtil.InitLibrary();
            new PBuildingManager().Register(AdvancedCritterSensorConfig.CreateBuilding());
        }
    }

    class DetailsScreenPatch {
        [HarmonyPatch(typeof(DetailsScreen), "OnPrefabInit")]
        public static class DetailsScreen_OnPrefabInit_Patch {
            public static void Postfix() {
                PUIUtils.AddSideScreenContent<AdvancedCritterSensorSideScreen>();
            }
        }
    }
}