
namespace notfound404.AdvancedCritterSensor {
	public static class AdvancedCritterSensorStrings {
		public static class BUILDINGS {
			public static class PREFABS {
				public static class ADVANCEDCRITTERSENSOR {
					public static LocString NAME = STRINGS.UI.FormatAsLink("Advanced Critter Sensor", AdvancedCritterSensorConfig.ID);
					public static LocString DESC = "Detecting specific critter populations can help adjust their automated feeding and care regiments.";
					public static LocString EFFECT = "Sends a " + STRINGS.UI.FormatAsAutomationState("Green Signal", STRINGS.UI.AutomationState.Active) + " or a " + STRINGS.UI.FormatAsAutomationState("Red Signal", STRINGS.UI.AutomationState.Standby) + " based on the number of eggs and critters in a room.";
				}
			}
		}
		public static class UI {
			public static class UISIDESCREENS {
				public static class ADVANCEDCRITTERSENSORSIDESCREEN {
					public static LocString TITLE = "Critter selection";
					public static LocString CRITTERCHECKBOX = "Count critters";
					public static LocString EGGCHECKBOX = "Count eggs";
				}
			}
		}
	}
}