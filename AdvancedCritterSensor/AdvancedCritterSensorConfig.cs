using PeterHan.PLib.Buildings;
using PeterHan.PLib.Core;
using UnityEngine;
using System.Linq;

namespace notfound404.AdvancedCritterSensor {
	public class AdvancedCritterSensorConfig : IBuildingConfig {
		internal const string ID = "AdvancedCritterSensor";

		internal static PBuilding AdvancedCritterSensor;

		internal static PBuilding CreateBuilding() {
			return AdvancedCritterSensor = new PBuilding(
				ID,
				AdvancedCritterSensorStrings.BUILDINGS.PREFABS.ADVANCEDCRITTERSENSOR.NAME
			) {
				AddAfter = LogicCritterCountSensorConfig.ID,
				Animation = "critter_sensor_kanim",
				AudioCategory = "Metal",
				AudioSize = "small",
				Category = "Automation",
				ConstructionTime = 30.0f,
				Decor = TUNING.BUILDINGS.DECOR.PENALTY.TIER0,
				Description = null,
				EffectText = null,
				Entombs = false,
				Floods = false,
				Height = 1,
				HP = 30,
				Ingredients = {
					new BuildIngredient(TUNING.MATERIALS.REFINED_METALS, tier: 0)
				},
				IsSolidTile = false,
				LogicIO = {
					LogicPorts.Port.OutputPort(
						LogicSwitch.PORT_ID,
						new CellOffset(0, 0),
						STRINGS.BUILDINGS.PREFABS.LOGICCRITTERCOUNTSENSOR.LOGIC_PORT,
						STRINGS.BUILDINGS.PREFABS.LOGICCRITTERCOUNTSENSOR.LOGIC_PORT_ACTIVE,
						STRINGS.BUILDINGS.PREFABS.LOGICCRITTERCOUNTSENSOR.LOGIC_PORT_INACTIVE,
						show_wire_missing_icon: true
					)
				},
				Placement = BuildLocationRule.Anywhere,
				SceneLayer = Grid.SceneLayer.Building,
				SubCategory = "sensors",
				Tech = "FinerDining",
				ViewMode = OverlayModes.Logic.ID,
				Width = 1,
				AlwaysOperational = true
			};
		}

		public override BuildingDef CreateBuildingDef() {
			LocString.CreateLocStringKeys(typeof(AdvancedCritterSensorStrings.BUILDINGS));
			SoundEventVolumeCache.instance.AddVolume("switchgaspressure_kanim", "PowerSwitch_on", TUNING.NOISE_POLLUTION.NOISY.TIER3);
			SoundEventVolumeCache.instance.AddVolume("switchgaspressure_kanim", "PowerSwitch_off", TUNING.NOISE_POLLUTION.NOISY.TIER3);
			GeneratedBuildings.RegisterWithOverlay(OverlayModes.Logic.HighlightItemIDs, ID);
			return AdvancedCritterSensor.CreateDef();
		}

		public override void ConfigureBuildingTemplate(GameObject go, Tag prefab_tag) {
			base.ConfigureBuildingTemplate(go, prefab_tag);
			AdvancedCritterSensor?.ConfigureBuildingTemplate(go);

			Storage storage = go.AddOrGet<Storage>();
			storage.allowItemRemoval = false;
			storage.showDescriptor = false;
			storage.storageFilters = TUNING.STORAGEFILTERS.BAGABLE_CREATURES.Concat(TUNING.STORAGEFILTERS.SWIMMING_CREATURES).ToList();
			storage.allowSettingOnlyFetchMarkedItems = false;
			storage.showInUI = true;
			storage.showCapacityStatusItem = false;
			storage.showCapacityAsMainStatus = false;
			go.AddOrGet<AdvancedCritterSensor>().manuallyControlled = false;
			go.AddOrGet<TreeFilterable>();
		}

		public override void DoPostConfigureComplete(GameObject go) {
			AdvancedCritterSensor?.DoPostConfigureComplete(go);
			AdvancedCritterSensor?.CreateLogicPorts(go);
			go.GetComponent<KPrefabID>().AddTag(GameTags.OverlayInFrontOfConduits);
		}

		public override void DoPostConfigurePreview(BuildingDef def, GameObject go) {
			AdvancedCritterSensor?.CreateLogicPorts(go);
		}

		public override void DoPostConfigureUnderConstruction(GameObject go) {
			AdvancedCritterSensor?.CreateLogicPorts(go);
		}
	}
}