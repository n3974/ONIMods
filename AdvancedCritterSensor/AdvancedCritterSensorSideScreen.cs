using PeterHan.PLib.Core;
using PeterHan.PLib.UI;
using UnityEngine;

namespace notfound404.AdvancedCritterSensor {
	public class AdvancedCritterSensorSideScreen : SideScreenContent
	{
		internal static readonly Vector2 ROW_SIZE = new Vector2(24.0f, 24.0f);

		public AdvancedCritterSensor targetSensor;

		private GameObject countCritter;

		private GameObject countEgg;

		protected override void OnPrefabInit()
		{
			BuildPanel();

			base.OnPrefabInit();
		}

		public override string GetTitle()
		{
			return "Critter filter";
		}

		public override bool IsValidForTarget(GameObject target)
		{
			return target.GetComponent<AdvancedCritterSensor>() != null;
		}

		public override void SetTarget(GameObject target)
		{
			base.SetTarget(target);
			targetSensor = target.GetComponent<AdvancedCritterSensor>();
			if (countCritter != null) {
				CheckCritter(targetSensor.countCritters);
			}
			if (countEgg != null) {
				CheckEgg(targetSensor.countEggs);
			}
		}

		public override void ClearTarget()
		{
			targetSensor = null;
		}

		private void BuildPanel()
		{
			if (ContentContainer != null)
			{
				Destroy(ContentContainer);
				ContentContainer = null;
			}

			var margin = new RectOffset(8, 8, 8, 8);
			var baseLayout = gameObject.GetComponent<BoxLayoutGroup>();
			if (baseLayout != null)
			{
				baseLayout.Params = new BoxLayoutParams()
				{
					Margin = margin,
					Direction = PanelDirection.Vertical,
					Alignment = TextAnchor.UpperLeft,
					Spacing = 8
				};
			}

			var mainPanel = new PPanel();

			var mainRow = new PPanel("Toggle Lock")
			{
				FlexSize = Vector2.right,
				Alignment = TextAnchor.MiddleLeft,
				Spacing = 10,
				Direction = PanelDirection.Vertical,
				Margin = margin
			};

			var initCritter = PCheckBox.STATE_UNCHECKED;
			if (targetSensor != null && targetSensor.countCritters) {
				initCritter = PCheckBox.STATE_CHECKED;
			}
			var critterChk = new PCheckBox("CritterCheck") {
				Text = AdvancedCritterSensorStrings.UI.UISIDESCREENS.ADVANCEDCRITTERSENSORSIDESCREEN.CRITTERCHECKBOX,
				CheckSize = ROW_SIZE,
				InitialState = initCritter,
				OnChecked = OnCheckCritter,
				TextStyle = PUITuning.Fonts.TextDarkStyle
			}.AddOnRealize((obj) => countCritter = obj);

			var initEgg = PCheckBox.STATE_UNCHECKED;
			if (targetSensor != null && targetSensor.countEggs) {
				initEgg = PCheckBox.STATE_CHECKED;
			}
			var eggChk = new PCheckBox("CritterCheck") {
				Text = AdvancedCritterSensorStrings.UI.UISIDESCREENS.ADVANCEDCRITTERSENSORSIDESCREEN.EGGCHECKBOX,
				CheckSize = ROW_SIZE,
				InitialState = initEgg,
				OnChecked = OnCheckEgg,
				TextStyle = PUITuning.Fonts.TextDarkStyle
			}.AddOnRealize((obj) => countEgg = obj);

			mainRow.AddChild(critterChk);
			mainRow.AddChild(eggChk);
			mainPanel.AddChild(mainRow);
			ContentContainer = mainPanel.Build();
			ContentContainer.SetParent(gameObject);
		}

		private void OnCheckCritter(GameObject source, int state) {
			targetSensor.countCritters = !targetSensor.countCritters;
			CheckCritter(targetSensor.countCritters);
		}

		private void OnCheckEgg(GameObject source, int state) {
			targetSensor.countEggs = !targetSensor.countEggs;
			CheckEgg(targetSensor.countEggs);
		}

		private void CheckCritter(bool status) {
			if (status) {
				PCheckBox.SetCheckState(countCritter, PCheckBox.STATE_CHECKED);
			} else {
				PCheckBox.SetCheckState(countCritter, PCheckBox.STATE_UNCHECKED);
			}
		}

		private void CheckEgg(bool status) {
			if (status) {
				PCheckBox.SetCheckState(countEgg, PCheckBox.STATE_CHECKED);
			} else {
				PCheckBox.SetCheckState(countEgg, PCheckBox.STATE_UNCHECKED);
			}
		}
	}
}